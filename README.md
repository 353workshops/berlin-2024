# Ultimate Go @ GopherCon Berlin 2024

Miki Tebeka
📬 [miki@ardanlabs.com](mailto:miki@ardanlabs.com), 𝕏 [@tebeka](https://twitter.com/tebeka), 👨 [mikitebeka](https://www.linkedin.com/in/mikitebeka/), ✒️[blog](https://www.ardanlabs.com/blog/)

#### Shameless Plugs

- [![](_extra/effective-go-recipes.png)](https://pragprog.com/titles/mtgo/effective-go-recipes/)
    - Use code `GopherCon_Europe_2024` to get 35% on this and my "Brain Teasers" books.
- [LinkedIn Learning Classes](https://www.linkedin.com/learning/instructors/miki-tebeka)
- [Ardan Labs Courses](https://www.ardanlabs.com/training/)

---

[Final Exercise](_extra/dld.md) - If you do it, send it as an email attachment.

---

### Agenda

- Programming vs engineering
- Polymorphism with interfaces
- Using generics to reduce code size
- Using concurrency
- Performance optimization (both CPU & Memory)

### Code

- [inc.go](inc/inc.go) - Values & pointers, stack
- [user.go](user/user.go) - Escape analysis
- [units.go](units/units.go) - Value vs pointer semantics
- [for.go](for/for.go) - `for` loop semantics
- [etl.go](etl/etl.go) - Starting concrete, moving to interfaces
- [sha1.go](sha1/sha1.go) - Combining interfaces
- [ml.go](ml/ml.go) - Using generics
- [stack.go](stack/stack.go) - Generic data structures
- [sched.go](sched/sched.go) - How the scheduler works (`GOMAXPROCS`)
- [chan.go](chan/chan.go) - Channel semantics
- [tokenizer.go](tokenizer/tokenizer.go) - Performance optimization


### Links

- [Effective Go](https://go.dev/doc/effective_go) - Keep this under your pillow
- [Go proverbs](https://go-proverbs.github.io/) - Reflect about them
- [gotraining](https://github.com/ardanlabs/gotraining) GitHub repository
    - [Profiling a Larger Web Service](https://github.com/ardanlabs/gotraining/blob/master/topics/go/profiling/project/README.md)
- [What is Software Engineering?](https://research.swtch.com/vgo-eng) by Russ Cox
- [sort examples](https://pkg.go.dev/sort/#pkg-examples) - Read and try to understand
- [interface implementation](https://github.com/golang/go/blob/master/src/runtime/runtime2.go#L205)
- [Why is my nil error value not equal to nil?](https://go.dev/doc/faq#nil_error) in the Go FAQ
- [Generics can make your Go code slower](https://planetscale.com/blog/generics-can-make-your-go-code-slower)
- [When to use generics](https://go.dev/blog/when-generics)
- [Generics tutorial](https://go.dev/doc/tutorial/generics)
- [Computer Latency at Human Scale](https://twitter.com/jordancurve/status/1108475342468120576/photo/1)
- [Garbage Collection in Go](https://www.ardanlabs.com/blog/2018/12/garbage-collection-in-go-part1-semantics.html)
- [A Guide to the Go Garbage Collector](https://go.dev/doc/gc-guide)
- [Scheduling in Go](https://www.ardanlabs.com/blog/2018/08/scheduling-in-go-part2.html) by Bill Kennedy
- [The Rules of Optimization Club](https://wiki.c2.com/?RulesOfOptimizationClub)
- [The race detector](https://go.dev/doc/articles/race_detector)
- [Rob Pike's 5 Rules of Programming](https://users.ece.utexas.edu/~adnan/pike.html)
- [Computer Latency at Human Scale](https://twitter.com/jordancurve/status/1108475342468120576)
- [High Performance Go Workshop](https://dave.cheney.net/high-performance-go-workshop/gophercon-2019.html)
- [Profile-guided optimization](https://go.dev/doc/pgo)
- [When NASA Lost a Spacecraft Due to a Metric Math Mistake](https://www.simscale.com/blog/nasa-mars-climate-orbiter-metric/)
- [automaxprocs](https://github.com/uber-go/automaxprocs) - Set `GOMAXPROCS` for containers/pods
- [Why are there nil channels in Go?](https://medium.com/justforfunc/why-are-there-nil-channels-in-go-9877cc0b2308)
- [Concurrency is not Parallelism](https://www.youtube.com/watch?v=cN_DpYBzKso) by Rob Pike
- [errgroup](https://pkg.go.dev/golang.org/x/sync/errgroup) - `sync.WaitGroup` with errors

### Data & Other

- [inc by value](_extra/inc-value.png)
- [inc by ptr](_extra/inc-ptr.png)
- [By value & by pointer](_extra/value-ptr.png)
- [CPU Cache](_extra/cpu-cache.png)
- [Error Cost](_extra/cost.png)
