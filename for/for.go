package main

import "fmt"

type player struct {
	name   string
	points int
}

func main() {
	players := []player{
		// Avengers
		{"Bruce", 11},   // Hulk
		{"Natasha", 19}, // Black widow
	}
	/* Value semantics "for" loop
	for _, p := range players {
		// p is a copy of the slice value
		p.points += 10
	}
	*/
	for i := range players {
		players[i].points += 10
	}
	fmt.Println(players)

	players1 := map[string]player{
		"hulk":        {"Bruce", 11},   // Hulk
		"black widow": {"Natasha", 19}, // Black widow
	}
	/* map value semantics
	for _, p := range players1 {
		p.points += 10
	}
	*/
	/* BUG: Won't compile
	for k := range players1 {
		players1[k].points += 10
	}
	*/

	// Pointer : Need to do read/modify/write
	for k, p := range players1 {
		p.points += 10
		players1[k] = p
	}

	fmt.Println(players1)

	for i := range 3 { // Go 1.22 +
		fmt.Println(i)
	}

	m := map[string]int{
		"x": 1,
	}

	for k := range m {
		m[k]++
		// v = m[k]
		// v = v + 1
		// m[k] = v
	}
	fmt.Println(m)
}
