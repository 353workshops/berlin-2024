package user_test

import (
	"fmt"

	"berlin24/user"
)

func ExampleNew() {
	u := user.New("elliot", "elliot@e-corp.com")
	fmt.Println(u)

	// Output:
	// &{elliot elliot@e-corp.com}
}

func ExampleNew1() {
	u := user.New1().WithName("elliot").WithEmail("elliot@e-corp.com")
	fmt.Println(u)

	// Output:
	// {elliot elliot@e-corp.com}
}
