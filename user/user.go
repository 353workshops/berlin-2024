package user

// Builder pattern
// u = New().WithEmail().WithName(...)
// Options
// u = New(WithName(), WithEmail())

func New1() User {
	return User{}
}

func (u User) WithName(name string) User {
	u.Name = name
	return u
}

func (u User) WithEmail(email string) User {
	u.Email = email
	return u
}

// New(name, email string) (*User, error)
// New(name, email string) *User
// New(name, email string) (User, error)
func New(name, email string) *User {
	// TODO: Validation
	u := User{
		Name:  name,
		Email: email,
	}

	return &u
}

type User struct {
	Name  string
	Email string
}

// go build -gcflags=-m

/* Value vs Pointer receiver
- Don't mix (user either value or pointer throughout)
	- Exception: Unmarshal
- If you have a lock -> must use pointer
- Think of time + 3 seconds vs points with 3 more likes
*/
