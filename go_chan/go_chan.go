package main

import (
	"fmt"
	"strings"
	"sync"
	"time"
)

func main() {
	// for i := range 3 {
	for i := 0; i < 3; i++ {
		go func() {
			fmt.Println("gr:", i)
		}()
	}

	fmt.Println("main")
	//	time.Sleep(time.Second)

	ch := make(chan int)
	go func() {
		ch <- 7 // send
	}()
	val := <-ch // receive
	fmt.Println("val:", val)

	go func() {
		for i := range 4 {
			ch <- i
		}
		close(ch)
	}()

	for v := range ch {
		fmt.Println("v:", v)
	}

	v := <-ch // ch is closed
	fmt.Println("v (closed):", v)

	v, ok := <-ch
	fmt.Println("v (closed):", v, "ok:", ok)

	fn := func() int {
		time.Sleep(100 * time.Millisecond) // simulate work
		return 42
	}

	fut := Future(fn)
	start := time.Now()
	fmt.Println("fut started at", start)
	out := <-fut
	fmt.Printf("out: %d, duration: %v\n", out, time.Since(start))

	fanOut()
	fanOutNoResult()
}

func fanOutNoResult() {
	text := "a little copying is better than a little dependency"
	words := strings.Fields(text)

	var wg sync.WaitGroup
	wg.Add(len(words))
	for _, w := range words {
		// wg.Add(1) // if you don't know size in advance
		go func() {
			defer wg.Done()
			w = strings.ToUpper(w)
			fmt.Println(w)
		}()
	}

	wg.Wait()
}

func fanOut() {
	text := "a little copying is better than a little dependency"
	words := strings.Fields(text)
	ch := make(chan string)
	for _, w := range words {
		go func() {
			w = strings.ToUpper(w)
			ch <- w
		}()
	}

	for range words {
		w := <-ch
		fmt.Println(w)
	}
}

// Exercise: Support errors from fn (func() (T, error))
func Future[T any](fn func() T) chan T {
	ch := make(chan T, 1) // buffered channel to avoid goroutine leak

	go func() {
		ch <- fn()
	}()

	return ch
}

/* Channel semantics
- send/receive will block until opposite operation(*)
	- channel with k sized buffer has k non-blocking sends
- receive from a closed channel will return zero value w/o blocking
- send to a closed channel will panic
- send/receive from a nil channel will block forever
*/
