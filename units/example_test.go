package units

import "fmt"

func ExampleValue() {
	v1 := Value{10, Centimeter}
	v2 := Value{3, Inch}

	// Value semantics
	fmt.Println(v1.Add(v2))

	/* Pointer semantics
	v1.Add(v2)
	fmt.Println(v1)
	*/

	// Output:
	// {17.62 Centimeter}
}

/*
- User value semantics as much as you can
- Pass pointers down the stack
- User pointer receivers when you have mutating methods
- Value to pointer is OK (unmarshal), pointer to value - no!
*/
