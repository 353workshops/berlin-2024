package units

type Unit struct {
	name string
}

func (u Unit) String() string {
	return u.name
}

var (
	Centimeter = Unit{"Centimeter"}
	Inch       = Unit{"Inch"}
)

// 1 inch = 2.54 centimeters

type Value struct {
	Amount float64
	Unit   Unit
}

func (v Value) Add(other Value) Value {
	a := other.Amount
	if v.Unit != other.Unit {
		key := regKey{other.Unit, v.Unit}
		a = registry[key](a)
	}
	return Value{v.Amount + a, v.Unit}
}

type regKey struct {
	from Unit
	to   Unit
}

var registry = map[regKey]func(float64) float64{
	{Centimeter, Inch}: func(v float64) float64 { return v / 2.54 },
	{Inch, Centimeter}: func(v float64) float64 { return v * 2.54 },
}
