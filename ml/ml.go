/*
	Generics (type constraints)

- Functions with similar code, different types
- Containers (data structures)
- GCShape stenciling
  - two concrete types are in the same gcshape grouping if and only if
    they have the same underlying type or they are both pointer types.
*/
package ml

import "fmt"

type Ordered interface {
	~int | ~float64 | ~string
}

func zero[T any]() (out T) {
	return
}

func Max[T Ordered](values []T) (T, error) {
	if len(values) == 0 {
		/*
			var zero T
			return zero, fmt.Errorf("Max of empty slice")
		*/
		return zero[T](), fmt.Errorf("Max of empty slice")
	}

	m := values[0]
	for _, v := range values[1:] {
		if v > m {
			m = v
		}
	}
	return m, nil
}

type Lesser interface {
	~int | ~float64
}

// T is a type constraint
func Relu[T Lesser](n T) T {
	if n < 0 {
		return 0
	}

	return n
}

/*
func ReluInt(n int) int {
	if n < 0 {
		return 0
	}

	return n
}

func ReluFloat64(n float64) float64 {
	if n < 0 {
		return 0
	}

	return n
}

*/
