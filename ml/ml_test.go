package ml

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestRelu(t *testing.T) {
	out := Relu(7)
	require.Equal(t, 7, out)
	out = Relu(-2)
	require.Equal(t, 0, out)

	require.Equal(t, time.January, Relu(time.January))

	/*
		fout := Relu(-2.0)
		require.Equal(t, 0.0, fout)
	*/
}

func TestMax(t *testing.T) {
	iOut, err := Max([]int{2, 3, 1})
	require.NoError(t, err)
	require.Equal(t, 3, iOut)

	fOut, err := Max([]float64{2, 3, 1})
	require.NoError(t, err)
	require.Equal(t, 3.0, fOut)

	//_, err = Max([]int{})
	_, err = Max[int](nil)
	require.Error(t, err)
}

/*
func TestReluInt(t *testing.T) {
	out := ReluInt(7)
	require.Equal(t, 7, out)
	out = ReluInt(-2)
	require.Equal(t, 0, out)
}

func TestReluFloat64(t *testing.T) {
	out := ReluInt(7)
	require.Equal(t, 7, out)
	out = ReluInt(-2)
	require.Equal(t, 0, out)
}

*/
