package main

import "fmt"

func main() {
	n := 41
	fmt.Printf("main: n=%d, addr=%p\n", n, &n)
	inc(&n)
	fmt.Printf("main: n=%d, addr=%p\n", n, &n)
	fmt.Println("n:", n)
}

func inc(n *int) {
	fmt.Printf("inc: n=%d, addr=%p\n", *n, n)
	*n++
	fmt.Printf("inc: n=%d, addr=%p\n", *n, n)
}

/*
Two types of data semantics:
Value semantics:
- default in Go
- mutation is isolated
- con: if I want a single point of truth

Pointer semantics:
- Everybody sees the same value
- con: synchronization, bugs
*/
