package tokenizer

import (
	"regexp"
	"strings"
)

var (
	// "Who's on first?" -> [Who s on first]
	wordRe   = regexp.MustCompile(`[a-zA-Z]+`)
	suffixes = []string{"ed", "ing", "s"}
)

// works -> work
// worked -> work
// working -> work
func Stem(word string) string {
	for _, s := range suffixes {
		if strings.HasSuffix(word, s) {
			return word[:len(word)-len(s)]
		}
	}

	return word
}

func initialSplit(text string) []string {
	// 90% of texts has less than 100 tokens
	fs := make([]string, 0, 100)
	i := 0
	for i < len(text) {
		// eat start
		for i < len(text) && !isLetter(text[i]) {
			i++
		}
		if i == len(text) {
			break
		}

		j := i + 1
		for j < len(text) && isLetter(text[j]) {
			j++
		}
		fs = append(fs, text[i:j])

		i = j
	}

	return fs
}

func isLetter(b byte) bool {
	return (b >= 'a' && b <= 'z') || (b >= 'A' && b <= 'Z')
}

func Tokenize(text string) []string {
	fs := initialSplit(text)
	tokens := make([]string, 0, len(fs))
	for _, tok := range fs {
		tok = strings.ToLower(tok)
		tok = Stem(tok)
		if tok != "" {
			tokens = append(tokens, tok)
		}
	}
	return tokens
}

/* Latency
- External (network) ms +
- Internal (GC) us
- Mechanical (cache miss) 10s ns

Most gain
- Design
- Algorithms & data structures
- Micro optimizations (they build up)
*/
