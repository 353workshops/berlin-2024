/*
	What is the sha1 signature of the uncompressed content of http.log.gz?

$ cat http.log.gz | gunzip | sha1sum
ef7dc39754fd23f7a1a8657e2ffda49edc49fff9 -
*/
package main

import (
	"compress/gzip"
	"crypto/sha1"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	fmt.Println(fileSHA1("http.log.gz"))
	fmt.Println(fileSHA1("sha1.go"))
}

// exercise: Only decompress if the file name ends with .gz
// $ cat http.log.gz | gunzip | sha1sum
// $ cat sha1.go | sha1sum

type gzWrapper struct {
	*gzip.Reader
	f *os.File
}

func (w *gzWrapper) Close() error {
	return errors.Join(
		w.Reader.Close(),
		w.f.Close(),
	)
}

func openFile(fileName string) (io.ReadCloser, error) {
	file, err := os.Open(fileName)
	if err != nil {
		return nil, err
	}
	if !strings.HasSuffix(fileName, ".gz") {
		return file, nil
	}

	// BUG: gzip.Reader.Close does not close underlying io.Reader
	// return gzip.NewReader(file)
	r, err := gzip.NewReader(file)
	if err != nil {
		file.Close()
		return nil, err
	}
	return &gzWrapper{r, file}, nil
}

// func fileSHA1(r io.Reader) (string, error) {
func fileSHA1(fileName string) (string, error) {
	r, err := openFile(fileName)
	if err != nil {
		return "", err
	}
	defer r.Close()

	w := sha1.New()
	if _, err = io.Copy(w, r); err != nil {
		return "", err
	}

	return fmt.Sprintf("%x", w.Sum(nil)), nil
}

/*
func fileSHA1(fileName string) (string, error) {
	file, err := os.Open(fileName)
	if err != nil {
		return "", err
	}
	defer file.Close()

	var r io.Reader = file
	if strings.HasSuffix(fileName, ".gz") {
		// BUG: r scope is the "if" statement
		// r, err := gzip.NewReader(file)
		r, err = gzip.NewReader(file)

		if err != nil {
			return "", err
		}
		defer r.Close()
		//		fmt.Printf("using gzip: %p\n", &r)
	}

	w := sha1.New()
	if _, err = io.Copy(w, r); err != nil {
		return "", err
	}

	return fmt.Sprintf("%x", w.Sum(nil)), nil
}
*/

type GZ struct {
	r io.Reader
}

func (gz *GZ) Close() error {
	if c, ok := gz.r.(io.Closer); ok {
		return c.Close()
	}
	return nil
}
