// ETL: Extract, Transform, Load
package main

import (
	"errors"
	"fmt"
	"io"
	"log"
)

type Record struct {
	Data string
}

type Victoria struct {
	Host string

	n int
}

// func (v *Victoria) Pull() (Record, error) {
func (v *Victoria) Pull(r *Record) error {
	v.n++

	if v.n > 10 {
		return io.EOF
	}

	r.Data = fmt.Sprintf("data #%d", v.n)
	log.Printf("Victoria: %s", r.Data)
	return nil
}

type Puller interface {
	Pull(*Record) error
}

type Henry struct {
	Host string
}

func (h *Henry) Store(d *Record) error {
	log.Printf("Henry: %s", d.Data)
	return nil
}

type Storer interface {
	Store(*Record) error
}

type William struct {
	Host string
}

func (h *William) Store(d *Record) error {
	log.Printf("William: %s", d.Data)
	return nil
}

func pull(p Puller, data []Record) (int, error) {
	for i := range data {
		err := p.Pull(&data[i])
		if errors.Is(err, io.EOF) {
			return i, nil
		}

		if err != nil {
			return i, err
		}
	}

	return len(data), nil
}

func store(s Storer, data []Record) (int, error) {
	for i := range data {
		if err := s.Store(&data[i]); err != nil {
			return i, err
		}
	}

	return len(data), nil
}

func Copy(s Storer, p Puller, batchSize int) error {
	data := make([]Record, batchSize)

	for {
		i, err := pull(p, data)
		if i == 0 {
			break
		}

		if err != nil {
			return err
		}

		if _, err := store(s, data[:i]); err != nil {
			return err
		}
	}

	return nil
}

func main() {
	v := Victoria{
		Host: "localhost:8000",
	}
	m := Henry{
		Host: "localhost:9000",
	}

	if err := Copy(&m, &v, 3); err != io.EOF {
		fmt.Println(err)
	}

	// Check of iface.data is nil
	var lm *LoginMessage
	fmt.Println(lm.Type())
	var t Typed = lm
	p, ok := t.(*LoginMessage)
	if !ok {
		fmt.Println("bad type")
	}
	fmt.Println("p:", p)

}

type Typed interface {
	Type() string
}

type LoginMessage struct{}

func (*LoginMessage) Type() string {
	return "login"
}

/* Interfaces
- Used for polymorphism
- Small (up to 5 methods)
- Accept interfaces, return types
- Mocking
	- Don't :)
	- Simulate error (http.Client.Transport)
*/

/*

type Sortable interface {
	Less(i, j int) bool
	Swap(i, j int)
	Len() int
}

func Sort(s Sortable) {
...
}

*/
